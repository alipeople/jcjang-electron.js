// Native
import { join } from 'path'
import { format } from 'url'
import cors from "cors";

// Packages
import { BrowserWindow, app, ipcMain, IpcMainEvent } from 'electron'
import isDev from 'electron-is-dev'
import prepareNext from 'electron-next'

import express from 'express'
const ioServer = express();

ioServer.use(cors())
import { createServer } from "http";
import { Server } from "socket.io";

const httpServer = createServer(ioServer);
const io = new Server(httpServer, {
  // ...
});

const socketsStatus : any = {};

io.on("connection", (socket) => {
  const socketId = socket.id;
    socketsStatus[socket.id] = {};
  
    console.log("connect");
  
    // socket.on("voice", function (data) {
  
    //   var newData = data.split(";");
    //   newData[0] = "data:audio/ogg;";
    //   newData = newData[0] + newData[1];
  
    //   for (const id in socketsStatus) {
  
    //     if (id != socketId && !socketsStatus[id].mute && socketsStatus[id].online)
    //       socket.broadcast.to(id).emit("send", newData);
    //   }
  
    // });
  
    socket.on("userInformation", function (data) {
      socketsStatus[socketId] = data;
      console.log(data);
      io.sockets.emit("usersUpdate",socketsStatus);
    });
  
  
    socket.on("disconnect", function () {
      delete socketsStatus[socketId];
    });
});

httpServer.listen(3000);

// Prepare the renderer once the app is ready
app.on('ready', async () => {
  await prepareNext('./renderer')

  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: false,
      contextIsolation: false,
      preload: join(__dirname, 'preload.js'),
      webSecurity : false,
    },
  })

  const url = isDev
    ? 'http://localhost:8000/'
    : format({
        pathname: join(__dirname, '../renderer/out/index.html'),
        protocol: 'file:',
        slashes: true,
      })
  console.log(`>>> listening on ${url}`)
  mainWindow.loadURL(url)
})

// Quit the app once all windows are closed
app.on('window-all-closed', app.quit)

// listen the channel `message` and resend the received message to the renderer process
ipcMain.on('message', (event: IpcMainEvent, message: any) => {
  console.log(message)
  setTimeout(() => event.sender.send('message', 'hi from electron'), 500)
})
