import { Button, Input } from "antd";
import Link from "next/link";
import { useEffect, useState } from "react";
import Layout from "../components/Layout";
import io from "socket.io-client";

const socket = io('ws://localhost:3000/')

const Home = () => {
  const [userStatus, setUserStatus ] = useState({
    microphone : false,
    mute: false,
    username: "user#" + Math.floor(Math.random() * 999999),
    online: false,
  })
  socket.emit("userInformation", userStatus);

  
  const handleOpenMike = () => {
    setUserStatus({
      ...userStatus,
      microphone : !userStatus.microphone
    })
  }

  const handleMute = () => {
    setUserStatus({
      ...userStatus,
      mute : !userStatus.mute
    })
  }

  useEffect(() => {
    socket.emit("userInformation", userStatus)
  }, [userStatus]);

  const handleToggleConnect = () => {
    setUserStatus({
      ...userStatus,
      online : !userStatus.online
    })
  }

  return (
    <Layout >
     <Input  placeholder="유저이름"/>
     <Button onClick={handleOpenMike} type="primary">마이크 열기</Button>
     <Button onClick={handleMute} type="primary">음소거</Button>
     <Button onClick={handleToggleConnect} type="primary">연결</Button>
    </Layout> 
  )
}


export default Home;